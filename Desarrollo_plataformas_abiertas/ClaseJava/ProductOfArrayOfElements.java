import java.util.Scanner;
public class ProductOfArrayOfElements {
   public static void main(String args[]){
      System.out.println("Tamaño del arreglo :: ");
      Scanner s = new Scanner(System.in);
      int size = s.nextInt();
      int myArray[] = new int [size];
      int product = 1;
      System.out.println("Ingresa número ");
      for(int i=0; i<size; i++){
         myArray[i] = s.nextInt();
        product = product * myArray[i]; 
      }
      System.out.println("El producto es ::"+product);
   }
}